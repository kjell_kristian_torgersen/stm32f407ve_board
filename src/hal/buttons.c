/*
 * buttons.c
 *
 *  Created on: Oct 11, 2020
 *      Author: kjell
 */

#include "stm32f4xx_hal.h"
#include "drivers/flash/flash.h"
int clicks1 = 0, clicks2 = 0;
int state1 = 0;
int state2 = 0;

void leds_set(int i);
extern UART_HandleTypeDef huart1;

void buttons_update(void)
{
	if (state1) {
		if (HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3)) {
			clicks1++;
			leds_set(clicks1);

			//flash_writePage(0, 0, "Hello World!\r\n", 15);

			state1 = 0;
		}
	} else {
		if (!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3)) {
			state1 = 1;
		}
	}

	if (state2) {
		if (HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_4)) {
			state2 = 0;
			//char buf[15];
			//flash_readPage(0, 0, buf, 15);
			//HAL_UART_Transmit(&huart1, buf, 15, 10);
		}
	} else {
		if (!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_4)) {
			state2 = 1;
		}
	}
}
