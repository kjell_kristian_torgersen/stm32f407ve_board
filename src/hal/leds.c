#include "stm32f4xx_hal.h"

void leds_set(int i)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, !(i & 1));
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, !(i & 2));
}
