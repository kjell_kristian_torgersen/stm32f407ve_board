#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "stm32f4xx_hal.h"

#include "api/delay.h"
#include "api/gpio.h"
#include "api/timer.h"
#include "drivers/ds18b20/ds18b20.h"
#include "drivers/flash/flash.h"
#include "hal/uart.h"
#include "ow_it.h"
#include "drivers/lcd/lcd.h"
#include "drivers/am2320/am2320.h"
#include "stm32-implementation/init.h"
#include "../../unused/onewire.h"

#define AM2320_ADDRESS 0x5C

extern uint32_t * adc_newValues(void);
extern void adc_start(void);

extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart1_tx;
extern SPI_HandleTypeDef hspi1;
extern TIM_HandleTypeDef htim1;
extern UART_HandleTypeDef huart1;
extern I2C_HandleTypeDef hi2c1;
char data[128];

uint8_t rxbuf[64];
uint8_t txbuf[256];


int32_t sum = 0;
uint32_t count = 0;
bool newValues = false;
static uint32_t timer = 0;
extern uint32_t ticks;
int32_t first = INT32_MIN;
uint8_t rxbyte;

static uart_t *uart;
extern int clicks1, clicks2;
extern TIM_HandleTypeDef htim7;
i2c_t * i2c;
/*void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
	sum += HAL_ADC_GetValue(hadc);
	count++;
	if (count >= 10000) {
		value = sum;

		sum = 0;
		count = 0;
		newValues = true;
	}
	//HAL_ADC_Start_IT(&hadc1);
}*/

uint8_t sensors[5][8];

void ds18b20_init(ds18b20_t *this, uint8_t port, uint8_t pin);


void app_main_setup(void)
{
	//HAL_UART_Receive_DMA(&huart1, rxbuf, sizeof(rxbuf));
	delay_init();
	HAL_TIM_Base_Start(&htim1);
	HAL_TIM_OC_Start_IT(&htim1, TIM_CHANNEL_1);
	uart = uart_init(&huart1, 115200, rxbuf, sizeof(rxbuf), txbuf, sizeof(txbuf));
	ow_it_init(0, 0);
	gpio_config(0, 1, GPIO_OUTPUT);
	//lcd_init2();
	i2c = i2c_init();
	//lcd_cls();
	adc_start();

}

void buttons_update(void);

uint16_t gettemp(ds18b20_t *this);
int uart_printf(const char *__restrict fmt, ...)
{
	char buf[256];
	va_list args;
	va_start(args, fmt);

	vsnprintf(buf, sizeof(buf), fmt, args);
	return uart_write(uart, buf, strlen(buf));
}

uint8_t buf[16];
uint8_t cmd = 0x33;
uint32_t i2ctimer = 0;
float adctemp = 0;
void app_main_loop(void)
{
	//uint8_t b;
	/*if (uart_read(uart, &b, 1)) {

		uart_write(uart, &b, 1);
		//lcd_putchar(b);
	}*/

	uint32_t * values = adc_newValues();
	if (values) {
		newValues = false;
		float voltage = 3.3 * values[3] / (4095.0 * 10000.0);
		adctemp = (voltage - 0.76) / 0.0025 + 25;
		//uart_printf("%f %f %i %i\n\r", HAL_GetTick() / 1000.0f, temp, clicks1, clicks2);
	}

	if (timer_ms() - timer >= 1000) {
		timer = timer_ms();
		int16_t temp;
		uint16_t hum;
		am2320_getTemperatureAndHumidity(i2c, AM2320_ADDRESS, &temp, &hum);
		uart_printf("%0.1f %0.1f %0.2f\n", temp / 10.0f, hum / 10.0f, adctemp);
	}

	/*if (timer_ms() - i2ctimer >= 3000) {
		i2ctimer = timer_ms();
		uart_printf("Scanning for I2C devices: ");
		for(int i = 0; i < 128; i++) {
			delay_ms(1);
			if(i2c_scan(i)) {
				uart_printf(" %02X", i);
			}
		}
		uart_printf("\r\n");
	}*/


	/*uart_printf("\r\n");

	 uart_printf("Scanning I2C bus:\r\n");
	 HAL_StatusTypeDef result;
	 uint8_t i;
	 for (i=1; i<128; i++)
	 {

	 result = HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t)(i<<1), 2, 2);
	 if (result != HAL_OK) // HAL_ERROR or HAL_BUSY or HAL_TIMEOUT
	 {
	 uart_printf("."); // No ACK received at that address
	 }
	 if (result == HAL_OK)
	 {
	 uart_printf("0x%X", i); // Received an ACK at that address
	 }
	 }
	 uart_printf("\r\n");*/
	/*switch (state) {
	 case 0:
	 if (timer_ms() - timer >= 1000) {
	 timer = timer_ms();
	 if (ow_it_done()) {
	 ow_it_reset();
	 timer = timer_ms();
	 state = 1;
	 }
	 }
	 break;
	 case 1:
	 if (timer_ms() - timer > 1) {
	 if (ow_it_done()) {
	 bool presence = !ow_it_bit();
	 uart_printf("presence = %i\r\n", presence);
	 ow_it_write(&cmd, 1);
	 state = 2;
	 }
	 }
	 break;
	 case 2:
	 if (ow_it_done()) {
	 ow_it_read(buf, 9);
	 state = 3;
	 }

	 break;
	 case 3:
	 if (ow_it_done()) {
	 for(int i = 0; i < 9; i++) {
	 uart_printf("%02X ", buf[i]);
	 }
	 uart_printf("\r\n");
	 state = 0;
	 }
	 break;
	 }*/

	/*uint8_t gSensorIDs[5][OW_ROMCODE_SIZE];
	 int nfound = ow2_searchSensors(gSensorIDs, 5);
	 uart_printf("nfound = %i\r\n", nfound);
	 for(int i = 0; i < nfound; i++) {
	 uart_printf("%i: ", i);
	 for(int j = 0; j < 8; j++) {
	 uart_printf("%02X", gSensorIDs[i][j]);
	 }
	 uart_printf("\r\n");
	 }



	 gpio_setPin(0, 1);*/

	//uint8_t ret[9];
	//uint16_t temp = gettemp(&d);
	/*	ow2_gettemp(ret);

	 for(int i = 0; i < 9; i++) {
	 snprintf(buf, sizeof(buf), "%02X ", ret[i]);
	 uart_write(uart, buf, strlen(buf));
	 }
	 uart_write(uart, "\r\n", 2);
	 uint16_t temp = ret[0] + ((uint16_t)ret[1] << 8);
	 snprintf(buf, sizeof(buf), "T = %f C\r\n", temp / 16.0);
	 uart_write(uart, buf, strlen(buf));*/

	//delay_ms(1000);
	//delay_ms(100);
	/*for(int i = 0; i < 1000; i++) {
	 gpio_config(0, 0, GPIO_INPUT);
	 delay_us(1);
	 gpio_config(0, 0, GPIO_OUTPUT);
	 delay_us(1);
	 }*/
	//HAL_Delay(1000);
	//HAL_UART_Transmit_DMA(&huart1, data, strlen((char*)data));
	//HAL_UART_Transmit(&huart1, data, strlen((char*)data), 100);
	//HAL_UART_Receive_IT(&huart1, &rxbyte, 1);
	/*if(HAL_GetTick() - timer >= 1000) {
	 timer = HAL_GetTick();
	 snprintf(data, sizeof(data), "%li\r\n", ticks);
	 ticks=0;
	 HAL_UART_Transmit(&huart1, data, strlen((char*) data), 100);
	 }*/
	//flash_init();
	//buttons_update();
	/*if (newValues) {
	 newValues = false;
	 float voltage = 3.3 * value / 40950000.0;
	 float temp = (voltage-0.76)/0.0025+25;
	 snprintf(data, sizeof(data), "%f %f %i %i\r\n", HAL_GetTick()/1000.0f, temp, clicks1, clicks2);
	 uart_write(uart, data, strlen(data));
	 //HAL_UART_Transmit(&huart1, data, strlen((char*) data), 100);

	 }
	 */

	/*int found = ow_search_sensors(&ow, sensors, 5);
	 //printf("found = %i\n", found);
	 delay_ms(100);*/
	//memset(data, 0, sizeof(data));
}

