#include <stdint.h>

#include "stm32f4xx_hal.h"

#define ADC_CHANNELS 4

extern ADC_HandleTypeDef hadc1;

static uint16_t adc_samples[ADC_CHANNELS];
static uint32_t adc_sums[ADC_CHANNELS];
static uint32_t counts=0;
static uint32_t adc_values[ADC_CHANNELS];

static uint32_t * newValues = NULL;

void adc_start(void)
{
	volatile HAL_StatusTypeDef status = HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_samples, sizeof(adc_samples));
	(void)status;
	//HAL_ADC_Start_IT(&hadc1);
}

void adc_process(void)
{
	for(int i = 0; i < 4; i++) {
		adc_sums[i] += adc_samples[i];
	}
	counts++;
	if(counts >= 10000) {
		counts = 0;
		for(int i = 0; i < ADC_CHANNELS; i++) {
			adc_values[i] = adc_sums[i];
			adc_sums[i] = 0;
			newValues = adc_values;
		}
	}
}

uint32_t * adc_newValues(void)
{
	uint32_t * ret = newValues;
	newValues = NULL;
	return ret;
}
