/*
 * uart.h
 *
 *  Created on: Oct 8, 2020
 *      Author: kjell
 */

#ifndef INC_HAL_UART_H_
#define INC_HAL_UART_H_


#include "int.h"

typedef struct uart uart_t;

uart_t * uart_init(UART_HandleTypeDef * huart, uint32_t baudRate, void * txbuf, sz_t txbufSize, void * rxbuf, sz_t rxbufSize);
sz_t uart_write(uart_t * this, const void * data, sz_t count);
sz_t uart_read(uart_t * this, void * data, sz_t count);


#endif /* INC_HAL_UART_H_ */
